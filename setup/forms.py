# -*- coding: utf-8 -*-
from django import forms
from popebu import messages
from django.utils.translation import ugettext_lazy as _
import MySQLdb


class SetupForm(forms.Form):
    debug = forms.BooleanField(
        label='Debug',
        initial=False,
        help_text='Com o Debug ativado os erros encontrados na compilação serão renderizados para o browser. Não recomendado para sites em produção pois exibide dados do servidor da aplicação.',
        required=False)
    nome = forms.CharField(
        label='Nome',
        help_text='Nome do site.')
    allowed_hosts = forms.URLField(
        label='Domínio',
        initial='http://localhost:8000',
        help_text='Domínio utilizado pelo site, como http://www.example.com')
    db_nome = forms.CharField(
        label='Banco de dados',
        help_text='Nome do banco de dados.',)
    db_user = forms.CharField(
        label='Usuário',
        help_text='Usuário para conexão no banco de dados.',)
    db_senha = forms.CharField(
        label='Senha',
        help_text='Senha de acesso do usuário do banco de dados.',
        widget=forms.PasswordInput())
    db_host = forms.CharField(
        label='Endereço',
        help_text='Endereço do banco de dados. Deixe em branco caso seja um servidor local.',
        required=False)
    db_porta = forms.CharField(
        label='Porta',
        help_text='Porta em que o serviço está em execução. Deixe em branco caso o banco esteja rodando na porta padrão (Mysql default: 3306).',
        required=False)
    recaptcha_public_key = forms.CharField(
        label='Recaptcha public key',
        help_text='Chave pública do ReCaptcha.',
        required=False)
    recaptcha_private_key = forms.CharField(
        label='Recaptcha private key',
        help_text='Chave privada do ReCaptcha.',
        required=False)
    solr_url = forms.CharField(
        label='Solr URL',
        help_text='Endereço do completo do servidor Solr, incluindo a porta em que o serviço está sendo executado.',
        required=False)
    memcached_url = forms.CharField(
        label='Memcached URL',
        help_text='Endereço do completo do servidor Memcached, incluindo a porta em que o serviço está sendo executado.',
        required=False)
    superuser_username = forms.CharField(
        label='Usuário',
        help_text='Username de super usuário.')
    superuser_senha = forms.CharField(
        label='Senha',
        help_text='Senha do super usuário.',
        widget=forms.PasswordInput())
    superuser_email = forms.EmailField(
        label='Email',
        help_text='Email do super usuário.')
    superuser_email_confirm = forms.EmailField(
        label='Confirmação do email',
        help_text='Redigite o email novamente.')
    
    def clean_superuser_email_confirm(self):
        cleaned_data = self.cleaned_data
        if 'superuser_email_confirm' in cleaned_data:
            if cleaned_data['superuser_email'] != cleaned_data['superuser_email_confirm']:
                raise forms.ValidationError(_(messages.EMAIL_ERROR))
        return cleaned_data['superuser_email_confirm']
    
    def clean_db_senha(self):
        cleaned_data = self.cleaned_data   
        if 'db_nome' in cleaned_data and 'db_user' in cleaned_data and 'db_senha' in cleaned_data: 
            params = {
                'db': cleaned_data.get('db_nome'),
                'host': cleaned_data.get('db_host', ''),
                'port': cleaned_data.get('db_porta', 3306),
                'user': cleaned_data.get('db_user'),
                'passwd': cleaned_data.get('db_senha'),                  
            }
            try:
                db=MySQLdb.connect(**params)
            except:
                raise forms.ValidationError(_(messages.INVALID_CONNECTION))
        return cleaned_data['db_senha']