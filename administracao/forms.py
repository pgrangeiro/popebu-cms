from administracao.models import Postagem
from django import forms
from datetime import datetime


class PostagemForm(forms.ModelForm):
    class Meta:
        model = Postagem
        
    def __init__(self, *args, **kwargs):
        initial = kwargs.setdefault('initial', {})
        initial['dt_atualizacao'] = datetime.now()
        super(PostagemForm, self).__init__(*args, **kwargs)