# -*- coding: utf-8 -*-
from administracao.models import Postagem, Categoria
from django.contrib import admin
from django.contrib.sites.models import Site
from django.contrib.sites import admin as site_admin
from administracao.forms import PostagemForm


class PostagemAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Destaque', {'fields': ('imagem',)}),
        (None, {'fields': (
            ('titulo', 'rascunho'),
            'conteudo',
            'categoria',
            'tags',)}),
        ('Registro', {'fields': (
            'dt_criacao', 
            'dt_atualizacao',)}
        )
    )
    add_fieldsets = (
        ('Destaque', {'fields': ('imagem',)}),
        (None, {'fields': (
            ('titulo', 'rascunho'),
            'conteudo',
            'categoria',
            'tags',)}),
        ('Registro', {'fields': ('dt_criacao', )}
        )
    )
    list_display = ('titulo', 'resumo', 'usuario', 'categoria', 'tags', 'dt_criacao', 'dt_atualizacao')
    search_fields = ('titulo', 'resumo', 'tags')
    list_filter = ('categoria', 'usuario')
    form = PostagemForm

    def save_model(self, request, obj, form, change):
        obj.usuario = request.user
        super(PostagemAdmin, self).save_model(request, obj, form, change)

    def get_fieldsets(self, request, obj=None):
        if not obj:
            return self.add_fieldsets
        return super(PostagemAdmin, self).get_fieldsets(request, obj)


class SiteAdmin(site_admin.SiteAdmin):
    def has_add_permission(self, request):
        return False

admin.site.register(Categoria)
admin.site.register(Postagem, PostagemAdmin)
admin.site.unregister(Site)
admin.site.register(Site, SiteAdmin)
