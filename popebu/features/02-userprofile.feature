# language: pt-br
Funcionalidade: Cadastro de usuario
    Para criar um usuario
    Enquanto Master 
    
    Cenario: Criar um usuario com sucesso
        Dado que estou na pagina inicial
        Clico no link 'Usuários'
        E clico no botao adicionar
        E preencho 'username' com 'XPTO1'
        E seleciono o arquivo 'perfil.jpg' no campo 'foto'
        E preencho 'biografia' com 'Yo!'
        E preencho 'first_name' com 'Nome'
        E preencho 'last_name' com 'Sobrenome'
        E preencho 'email' com 'email@mail.com'
        E seleciono 'groups_old' com 'Redator'
        E clico no link 'Adicionar'
        E clico no botao 'Save'
        Entao vejo a mensagem 'was added successfully'
    
    Cenario: Criar um usuario com sucesso (somente campos obrigatorios)
        Dado que estou na pagina inicial
        Clico no link 'Usuários'
        E clico no botao adicionar
        E preencho 'username' com 'XPTO2'
        E preencho 'email' com 'email2@mail.com'
        E seleciono 'groups_old' com 'Redator'
        E clico no link 'Adicionar'
        E clico no botao 'Save'
        Entao vejo a mensagem 'was added successfully'
    
    Cenario: Criar um usuario sem sucesso (sem username)
        Dado que estou na pagina inicial
        Clico no link 'Usuários'
        E clico no botao adicionar
        E preencho 'email' com 'email3@mail.com'
        E seleciono 'groups_old' com 'Redator'
        E clico no link 'Adicionar'
        E clico no botao 'Save'
        Entao vejo a mensagem 'field is required'
    
    Cenario: Criar um usuario sem sucesso (sem email)
        Dado que estou na pagina inicial
        Clico no link 'Usuários'
        E clico no botao adicionar
        E preencho 'username' com 'XPTO3'
        E seleciono 'groups_old' com 'Redator'
        E clico no link 'Adicionar'
        E clico no botao 'Save'
        Entao vejo a mensagem 'field is required'
    
    Cenario: Criar um usuario sem sucesso (email ja cadastrado)
        Dado que estou na pagina inicial
        Clico no link 'Usuários'
        E clico no botao adicionar
        E preencho 'username' com 'XPTO3'
        E preencho 'email' com 'email@mail.com'
        E seleciono 'groups_old' com 'Redator'
        E clico no link 'Adicionar'
        E clico no botao 'Save'
        Entao vejo a mensagem 'must be unique'
    
    Cenario: Alterar um usuario com sucesso
        Dado que estou na pagina inicial
        Clico no link 'Usuários'
        E clico no link 'XPTO1'
        E clico no botao 'Save'
        Entao vejo a mensagem 'was changed successfully'
        
    Cenario: Excluir usuario com sucesso
        Dado que estou na pagina inicial
        Clico no link 'Usuários'
        E clico no link 'XPTO1'
        E clico no botao apagar
        E confirmo a exclusao
        Entao vejo a mensagem 'was deleted successfully'
    
    