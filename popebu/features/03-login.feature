# language: pt-br
Funcionalidade: Login
    Para acessar o ambiente
    Enquanto Redator
    
    Cenario: Logar sem sucesso (exibir captcha apos 3 tentativa)
        Logout
        Dado que estou na pagina inicial
        E preencho 'username' com 'Redator'
        E preencho 'password' com 'a'
        E clico no botao 'Log in'
        E preencho 'username' com 'Redator'
        E preencho 'password' com 'a'
        E clico no botao 'Log in'
        E preencho 'username' com 'Redator'
        E preencho 'password' com 'a'
        E clico no botao 'Log in'
        Entao vejo o campo 'recaptcha_response_field'