# -*- coding: utf-8 -*-

USER_LOGIN_ERROR = 'Please enter the correct %(username)s and password for a staff account. Note that both fields may be case-sensitive.'

UNIQUE_FIELD = 'This field must be unique.'

EMAIL_ERROR = 'Email fields do not match.'

INVALID_CONNECTION = 'Unable to connect on database. Are you sure that connection parameters are correct?'
