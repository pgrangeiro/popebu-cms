# Bem vindo!

Popebu é um CMS opensource baseado no [Django](http://www.djangoproject.com). Sinta-se a vontade para fazer um fork ou relatar um incidente.

## Requisitos
- Python 2.7+
- Pip
- Mysql
- mysql-connector-python
- Virtualenv (opcional)

## Instalação
- Depois de baixar o código do projeto e descompactá-lo em uma pasta, é necessário instalar as bibliotecas de dependência do projeto. Caso tenha optado por utilizar o virtualenv, crie e ative o seu ambiente antes da instalação das bibliotecas.
```
$ pip install -r popebu/conf/requirements/requirements.txt
```
- Depois de instalar as bibliotecas, inicie o servidor da aplicação. Caso esteja desenvolvendo um projeto local, o servidor de desenvolvimento do Django pode ser utilizado.
```
$ python manage.py runserver
```
- Em um primeiro acesso, é exibida a página de setup do Popebu onde você deve informar alguns dados básicos para a configuração inicial do projeto.
- Depois de concluir a configuração do projeto com sucesso, reinicie o servidor da aplicação.

Para maiores informações, leia a [documentação](http://popebu-cms.readthedocs.org/pt_BR/latest/) do projeto.