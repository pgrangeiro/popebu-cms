from django import template
from auth.models import UserProfile
from administracao.models import Categoria

register = template.Library()

@register.inclusion_tag('perfil/sidebar.html', takes_context=True)
def get_redatores(context):
    result = UserProfile.objects.filter(
        is_active=True,
        groups__id=3)
    return {
        'result': result,
        'MEDIA_URL': context['MEDIA_URL']}


@register.inclusion_tag('categoria/sidebar.html')
def get_categorias():
    result = Categoria.objects.all()
    return {'result': result}