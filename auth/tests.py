from django.test import TestCase, RequestFactory
from django.contrib.admin.sites import AdminSite
from auth.models import UserProfile
from django.core.urlresolvers import reverse
from django.test.client import Client
from auth.admin import UserProfileAdmin
from auth.forms import *
from django.contrib.sessions.middleware import SessionMiddleware


class UserProfileTest(TestCase):
    fixtures = (
        'popebu/conf/data/00-grupos.json', 
        'popebu/conf/data/01-user.json',)
    
    def setUp(self):
        self.site = AdminSite()
        self.admin = UserProfileAdmin(UserProfile, self.site)
        
        self.factory = RequestFactory()        
        self.form = UserProfileForm
        
        self.client = Client()
        self.client.login(username='Master', password='admin')
        
    def test_request_add(self):
        url = reverse('admin:auth_userprofile_add')
        self.request = self.factory.get(url)
        self.request.user = UserProfile.objects.get(id=1)
        self.request.POST = {
            'username': 'Teste', 
            'email': 'email@teste.com'}
        response = self.client.post(url, self.request.POST)
        self.assertIn(response.status_code, [200, 302])
        
    def test_request_change(self):
        obj = UserProfile.objects.get(id=2)
        url = reverse('admin:auth_userprofile_change', args=[obj.id])
        self.request = self.factory.get(url)
        self.request.user = UserProfile.objects.get(id=1)
        self.request.POST = {
            'username': 'Teste', 
            'email': 'email@teste.com'}
        response = self.client.post(url, self.request.POST)
        self.assertIn(response.status_code, [200, 302])
        

class AdminAuthenticationTest(TestCase):
    fixtures = (
        'popebu/conf/data/00-grupos.json', 
        'popebu/conf/data/01-user.json',)
    
    def setUp(self):            
        self.form = AdminAuthenticationForm
        
        self.factory = RequestFactory()    
        self.request = self.factory.get('/admin/')
                
    def test_authentication(self):
        url = '/admin/'
        data = {
            'username': 'Master',
            'password': 'admin'}

        middleware = SessionMiddleware()
        middleware.process_request(self.request)
        self.request.session.save()
        
        form = self.form(self.request, data=data)
        self.assertFalse(form.is_valid())
        
        
    